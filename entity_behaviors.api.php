<?php

/**
 * @file
 * API documentation for the entity_behaviors module.
 */

/**
 * Alter the selector items.
 *
 * @param array $selectors
 *   An array of selector classes.
 *
 * @see entity_behaviors_selectors()
 */
function hook_entity_behaviors_selectors_alter(&$selectors) {
  $selectors['node'] = 'custom-node-';
}
