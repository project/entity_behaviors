The Height sub-module allows a user to change the height of a paragraph item.

## Installation

**Make sure you have followed the instructions in README.txt in the parent module.**

No extra modules are required.
