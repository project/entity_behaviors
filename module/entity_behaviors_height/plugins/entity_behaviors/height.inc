<?php

/**
 * @file
 * Plugin for entity behaviors height.
 */
$plugin = array(
  'name' => 'height',
  'label' => t('Add height behaviors to entities.'),
  'handler' => 'entity_behaviors_height_behavior',
);

function entity_behaviors_height_behavior($selector, $entity) {
  $wrapper = entity_metadata_wrapper('field_collection_item', $entity);
  $height = $wrapper->{EBS_HEIGHT_FIELD}->value(array('sanitize' => TRUE));

  if (!empty($height)) {
    drupal_add_css($selector . ' {height: ' . $height . ';}', 'inline');
  }
}
