<?php

/**
 * @file
 * Admin include file for entity behaviors skrollr.
 */

/**
 * Menu callback function for settings form.
 */
function entity_behaviors_skrollr_settings_form($form, &$form_state) {
  $form['heading_mobile'] = array(
    '#type' => 'markup',
    '#markup' => t('<h2>Mobile</h2>'),
  );

  $form[EBS_SKROLLR_DISABLE_ON_MOBILE] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable on mobile'),
    '#description' => t('Scrollr will not be initialized on mobile devices.'),
    '#default_value' => variable_get(EBS_SKROLLR_DISABLE_ON_MOBILE, FALSE),
  );

  // Prepare a link for the init options heading.
  $url = 'https://github.com/Prinzhorn/skrollr#forceheighttrue';
  $link = l($url, $url, array('absolute' => TRUE));

  $form['heading_init_options'] = array(
    '#type' => 'markup',
    '#markup' => t('<h2>Skrollr init() options</h2><p>More info at !link.</p>', array('!link' => $link)),
  );

  $form[EBS_SKROLLR_SMOOTH_SCROLLING] = array(
    '#type' => 'checkbox',
    '#title' => t('smoothScrolling'),
    '#description' => t('Smooth scrolling smoothens your animations. When you scroll down 50 pixel the animations will transition instead of jumping to the new position.'),
    '#default_value' => variable_get(EBS_SKROLLR_SMOOTH_SCROLLING, TRUE),
  );

  $form[EBS_SKROLLR_SMOOTH_SCROLLING_DURATION] = array(
    '#type' => 'textfield',
    '#title' => 'smoothScollingDuration',
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => 'The number of milliseconds the animations run after the scroll position changed the last time.',
    '#default_value' => variable_get(EBS_SKROLLR_SMOOTH_SCROLLING_DURATION, 200),
  );

  $form[EBS_SKROLLR_CONSTANTS] = array(
    '#type' => 'textarea',
    '#title' => 'constants',
    '#rows' => 10,
    '#columns' => 40,
    '#description' => t('<p>An object containing integers as values. The keys can contain [a-z0-9_]. They do not need a leading underscore.</p><p>Example:</p><pre>{myconst: 300, myconst2:600}</pre>'),
    '#default_value' => variable_get(EBS_SKROLLR_CONSTANTS, ''),
  );

  $form[EBS_SKROLLR_SCALE] = array(
    '#type' => 'textfield',
    '#title' => 'scale',
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('<p>Default is 1. Smaller numbers such as .8 speed up the animation, Larger numbers such as 1.2 slow it down.</p><p>By default skrollr uses the largest key frame and makes document height + viewport height this high, thus the max possible scroll top offset. If your animation runs too fast or too slow, just adjust the scale value.</p><p> scale only affects keyframes in absolute mode.</p><p>When forceHeight is set to false, scale is ignored. scale affects constants as well. scale does only affect key frames in absolute mode, e.g. data-500 but not data-top.</p>'),
    '#default_value' => variable_get(EBS_SKROLLR_SCALE, 1),
  );

  $form[EBS_SKROLLR_FORCE_HEIGHT] = array(
    '#type' => 'checkbox',
    '#title' => t('forceHeight'),
    '#description' => t("<p>true: Make sure the document is high enough that all key frames fit inside.</p><p>Example: You use data-1000, but the content only makes the document 500px high. skrollr will ensure that you can scroll down the whole 1000px.</p><p>Or if you use relative mode, e.g. data-top-bottom, skrollr will make sure the bottom of the element can actually reach the top of the viewport.</p><p>false: Don't manipulate the document and just keep the natural scrollbar.</p>"),
    '#default_value' => variable_get(EBS_SKROLLR_FORCE_HEIGHT, TRUE),
  );

  $form[EBS_SKROLLR_MOBILE_CHECK] = array(
    '#type' => 'textarea',
    '#title' => 'mobileCheck',
    '#rows' => 10,
    '#columns' => 40,
    '#description' => t('<p>Example:</p><pre>function() {
return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
}</pre>'),
    '#default_value' => variable_get(EBS_SKROLLR_MOBILE_CHECK, ''),
  );

  $form[EBS_SKROLLR_MOBILE_DECELERATION] = array(
    '#type' => 'textfield',
    '#title' => 'mobileDeceleration',
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('The amount of deceleration for momentum scrolling on mobile devices. This options tells skrollr how fast or slow you want the scrolling to stop after the user lifted his finger.<br/>Set it to 1 to disable momentum scrolling.'),
    '#default_value' => variable_get(EBS_SKROLLR_MOBILE_DECELERATION, .004),
  );

  $form[EBS_SKROLLR_SKROLLR_BODY] = array(
    '#type' => 'textfield',
    '#title' => 'skrollrBody',
    '#size' => 60,
    '#maxlength' => 200,
    '#description' => t('<p>This option allows you to override the default id-selector used for supporting mobile browsers. It might come in handy when the element in question already has a assigned id or if you would like to define more then one skrollrBody depending on preceding JavaScript-logic.</p>'),
    '#default_value' => variable_get(EBS_SKROLLR_SKROLLR_BODY, 'skrollr-body'),
  );

  $empty = array();
  $form[EBS_SKROLLR_EDGE_STRATEGY] = array(
    '#type' => 'select',
    '#title' => t('edgeStrategy'),
    '#description' => "<p>This option specifies how to handle animations when the scroll position is outside the range on the keyframes (i.e. before the first or after the last keyframe).</p><p>One of three options are possible</p><p>set (default): When before/after the first/last keyframe, apply the styles of the first/last keyframe to the element.</p><p>ease: Same as set, but the values will be transformed using the given easing function.</p><p>reset: When before/after the first/last keyframe, apply the styles which the element had before skrollr did anything. This means resetting the class attribute as well as removing all styles which have been applied to the style property. This means the element won't have any skrollable-* CSS classes.</p>",
    '#options' => array('set' => 'set',
      'ease' => 'ease',
      'reset' => 'reset',
    ),
    '#default_value' => variable_get(EBS_SKROLLR_EDGE_STRATEGY, 'set'),
  );

  $form[EBS_SKROLLR_EDGE_STRATEGY_EASE_FUNCTION] = array(
    '#type' => 'text',
    '#title' => t('Function for edge strategy ease'),
    '#size' => 60,
    '#maxlength' => 200,
    '#description' => t('<p>Examples:</p><ul><li>linear</li><li>quadratic</li><li>cubic</li><li>sin (can be custom defined in easing settings)</li></ul></p>'),
    '#default_value' => variable_get(EBS_SKROLLR_EDGE_STRATEGY_EASE_FUNCTION, ''),
  );

  $form[EBS_SKROLLR_BEFORE_RENDER] = array(
    '#type' => 'textarea',
    '#title' => 'beforerender',
    '#rows' => 10,
    '#columns' => 40,
    '#description' => t("<p>A listener function getting called each time right before we render everything. The function will be passed an object with the following properties:</p><pre>
{
    curTop: 10, //the current scroll top offset
    lastTop: 0, //the top value of last time
    maxTop: 100, //the max value you can scroll to. curTop/maxTop will give you the current progress.
    direction: 'down' //either up or down
}</pre><p>Returning false will prevent rendering.</p>"),
    '#default_value' => variable_get(EBS_SKROLLR_BEFORE_RENDER, ''),
  );

  $form[EBS_SKROLLR_RENDER] = array(
    '#type' => 'textarea',
    '#title' => 'render',
    '#rows' => 10,
    '#columns' => 40,
    '#description' => t('<p>A listener function getting called right after we finished rendering everything. The function will be passed the same parameters as beforerender.</p><p>Example:</p><pre>
function(data) {
  //Log the current scroll position.
  console.log(data.curTop);
}</pre>'),
    '#default_value' => variable_get(EBS_SKROLLR_RENDER, ''),
  );

  $form[EBS_SKROLLR_EASING] = array(
    '#type' => 'textarea',
    '#title' => 'easing',
    '#rows' => 10,
    '#columns' => 40,
    '#description' => t('<p>Example:</p><pre>
sin: function(p) {
return (Math.sin(p * Math.PI * 2 - Math.PI/2) + 1) / 2;
},
cos: function(p) {
return (Math.cos(p * Math.PI * 2 - Math.PI/2) + 1) / 2;
}</pre>'),
    '#default_value' => variable_get(EBS_SKROLLR_EASING, ''),
  );

  return system_settings_form($form);
}
