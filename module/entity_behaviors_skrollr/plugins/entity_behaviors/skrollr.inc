<?php

/**
 * @file
 * Plugin for entity behaviors skrollr.
 */

$plugin = array(
  'name' => 'skrollr',
  'label' => t('Add skrollr behaviors to entities.'),
  'handler' => 'entity_behaviors_skrollr_behavior',
);

function entity_behaviors_skrollr_behavior($selector, $entity) {
  if (($library = libraries_load(EBS_SKROLLR_LIBRARY)) && !empty($library['loaded'])) {
    $options = array();
    $wrapper = entity_metadata_wrapper('field_collection_item', $entity);
    foreach ($wrapper->{EBS_SKROLLR_FIELD}->getIterator() as $delta => $value) {
      // Get data attributes.
      $data = $value->value();
      // Validate data.
      if (!entity_behaviors_skrollr_validate_data($data)) {
        continue;
      }
      // Sanitize data.
      list($attr_name, $attr_value) = entity_behaviors_skrollr_get_sanitized_data($data);
      // Fill in data attributes.
      $options[$attr_name] = $attr_value;
    }
    // If options is not empty then load library.
    if (!empty($options)) {
      $js_settings = array(
        'items' => array($selector => $options),
        'disableOnMobile' => variable_get(EBS_SKROLLR_DISABLE_ON_MOBILE, FALSE),
      );
      drupal_add_js(array('entity_behaviors_skrollr' => $js_settings), 'setting');

      entity_behaviors_skrollr_add_js();
    }
  }
}

/**
 * Add skrollr javascript, but just once.
 */
function entity_behaviors_skrollr_add_js() {
  $first_time = &drupal_static(__FUNCTION__, TRUE);

  if ($first_time) {
    // Add init options variable.
    entity_behaviors_skrollr_adding_init_options();
    // Add skrollr.js plugin.
    drupal_add_js(drupal_get_path('module', 'entity_behaviors_skrollr') . '/plugins/entity_behaviors/skrollr.js');

    // Do not repeat.
    $first_time = FALSE;
  }
}

/**
 * Add skrollr init options variable.
 *
 * These options will be passed to skrollr init() method.
 * More info: https://github.com/Prinzhorn/skrollr#skrollrinitoptions
 */
function entity_behaviors_skrollr_adding_init_options() {
  // Beginning of skrollr init options variable definition.
  $skrollr_init_js = 'var ' . EBS_SKROLLR_INIT_OPTIONS . ' = {';

  if (variable_get(EBS_SKROLLR_SMOOTH_SCROLLING, TRUE)) {
    $skrollr_init_js .= ' smoothScrolling : true, ';
  }

  if (variable_get(EBS_SKROLLR_SMOOTH_SCROLLING_DURATION, FALSE)) {
    $skrollr_init_js .= ' smoothScrollingDuration : ' . variable_get(EBS_SKROLLR_SMOOTH_SCROLLING_DURATION) . ', ';
  }

  if (variable_get(EBS_SKROLLR_CONSTANTS, FALSE)) {
    $skrollr_init_js .= ' constants : ' . variable_get(EBS_SKROLLR_CONSTANTS) . ', ';
  }

  if (variable_get(EBS_SKROLLR_SCALE, FALSE)) {
    $skrollr_init_js .= ' scale : ' . variable_get(EBS_SKROLLR_SCALE) . ', ';
  }

  if (!variable_get(EBS_SKROLLR_FORCE_HEIGHT, FALSE)) {
    $skrollr_init_js .= ' forceHeight : false, ';
  }

  if (variable_get(EBS_SKROLLR_MOBILE_CHECK, FALSE)) {
    $skrollr_init_js .= ' mobileCheck : ' . variable_get(EBS_SKROLLR_MOBILE_CHECK) . ', ';
  }

  $skrollr_init_js .= ' mobileDeceleration : ' . variable_get(EBS_SKROLLR_MOBILE_DECELERATION, .004) . ', ';

  if (variable_get(EBS_SKROLLR_SKROLLR_BODY, FALSE)) {
    $skrollr_init_js .= ' skrollrBody : ' . "'" . variable_get(EBS_SKROLLR_SKROLLR_BODY) . "'" . ', ';
  }

  $edge_strategy = variable_get(EBS_SKROLLR_EDGE_STRATEGY, "set");

  if ($edge_strategy == "'ease'") {
    $edge_strategy = variable_get(EBS_SKROLLR_EDGE_STRATEGY_EASE_FUNCTION, '');
  }

  $skrollr_init_js .= ' edgeStrategy : ' . "'" . $edge_strategy . "'" . ', ';

  if (variable_get(EBS_SKROLLR_BEFORE_RENDER, FALSE)) {
    $skrollr_init_js .= ' beforerender: ' . variable_get(EBS_SKROLLR_BEFORE_RENDER) . ',';
  }

  if (variable_get(EBS_SKROLLR_RENDER, FALSE)) {
    $skrollr_init_js .= ' render: ' . variable_get(EBS_SKROLLR_RENDER) . ',';
  }

  if (variable_get(EBS_SKROLLR_EASING, FALSE)) {
    $skrollr_init_js .= ' easing: {' . variable_get(EBS_SKROLLR_EASING) . '},';
  }

  // End of skrollr init options variable definition.
  $skrollr_init_js .= '};';

  // Adding skrollr init options variable.
  drupal_add_js($skrollr_init_js, array('type' => 'inline', 'scope' => 'header'));
}
