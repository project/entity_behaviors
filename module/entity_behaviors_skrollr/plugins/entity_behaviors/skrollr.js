/**
 * @file
 * Javascript for entity behaviors skrollr.
 */

(function ($) {

  Drupal.behaviors.entityBehaviorsSkrollr = {
    attach: function (context) {
      var items = this.getItems();

      // Set skrollr attributes and init skrollr.
      $.each(items, function (selector, options) {
        $(selector, context).attr(options);
        // Add class.
        $(selector, context).addClass('ebs-skrollr');
      });

      this.initScrollr();
    },
    getItems: function () {
      var items = [];

      var settings = Drupal.settings.entity_behaviors_skrollr;
      if (typeof settings != 'undefined' && typeof settings['items'] != 'undefined') {
        items = settings['items'];
      }

      return items;
    },
    initScrollr: function () {
      // If set so then do not initialize skrollr on mobile.
      if (this.disableOnMobile() && (/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)) {
        // Skip the skrollr initialization.
        return;
      }

      var s = skrollr.init(entityBehaviorsSkrollrInitOptions);
    },
    disableOnMobile: function () {
      var disableOnMobile = false;

      var settings = Drupal.settings.entity_behaviors_skrollr;
      if (typeof settings != 'undefined' && typeof settings['disableOnMobile'] != 'undefined') {
        disableOnMobile = settings['disableOnMobile']
      }

      return disableOnMobile;
    }
  };

})(jQuery);
