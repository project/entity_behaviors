The Skrollr sub-module integrates the Skrollr library into Paragraphs.

## Installation

**Make sure you have followed the instructions in README.txt in the parent module.**

The sub-module requires the following modules:

* [Libraries API](https://www.drupal.org/project/libraries)

The sub-module requires the Skrollr library, go download the skrollr.min.js from https://github.com/Prinzhorn/skrollr/tree/master/dist and place it in "sites/all/libraries/skrollr".

## Test Skrollr

Go to a paragraph and select "Skrollr" from the selection drop-down. To test that everything is working add the following data value:

- data-0="background-color:rgb(0,0,255);transform[bounce]:rotate(0deg);"

- data-500="background-color:rgb(255,0,0);transform[bounce]:rotate(360deg);"